package com.company.emptydemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.ConfigurationPropertiesScan;

@SpringBootApplication
@ConfigurationPropertiesScan
public class EmptyDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(EmptyDemoApplication.class, args);
    }
}
